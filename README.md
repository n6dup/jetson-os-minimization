# Jetson-OS-minimization

## sinit
We use [sinit](https://git.suckless.org/sinit/) as a minimal init system.
`conf/rc.init` and `conf/rc.shutdown` are sample sinit minimal init scripts.

### Deps
We use [smdev](https://git.suckless.org/smdev/) and [firmwared](https://github.com/teg/firmwared) to manage devices under sinit.

We also use [ubase](https://git.suckless.org/ubase/) at least to an extent.

All are compiled and installed into `/usr/local/bin` by default.  See `usrlocal.txt` for a full list of expected binaries/links.

## Bootloader Conf 
`/boot/extlinux/extlinux.conf` is the bootload config.
Our example copy is in `conf/extlinux.conf`
It defaults to booting into sinit in RO mode but has sinit in RW and normal systemd in RO and RW modes as options as well.

## Step by Step

This is a *very* rough step by step based on bash history.


```
#update, remove X, reinstall cuda
apt update && apt dist-upgrade
apt install overlayroot 
sudo apt-get purge -y libx11.* libqt.* libgtk.*
apt autoremove
apt install python-libnvinfer python3-libnvinfer tensorrt libnvinfer-samples libcudnn7 cuda-cublas-10-0 cuda-cudart-10-0 cuda-repo-l4t-10-0-local-10.0.166 libvisionworks-repo libvisionworks libvisionworks-sfm libvisionworks-sfm-repo libvisionworks-tracking-repo cuda-toolkit-10-0 cuda-samples-10-0 libgl1-mesa-dev libglvnd0 libglvnd-dev libglvnd-core-dev python-pip python-opencv

#optional for debugging cuda/opencv over remote
apt install xrdp

#locate is nice
apt install mlocate
updatedb

#edit or copy extlinux and edit overlayroot.conf (or copy once we grab it -- I forgot to)
vim /etc/overlayroot.conf 
vim /etc/extlinux/extlinux.conf 

#install sinit
cd ~
git clone git://git.suckless.org/sinit
cd sinit/
make
make install

#copy or edit init file
vim /bin/rc.init
vim /bin/rc.shutdown
chmod 755 /bin/rc.init /bin/rc.shutdown

#install ubase
cd ~
git clone git://git.suckless.org/ubase
cd ubase/
make ubase-box
make ubase-box-install

#Update initrd so overlayroot runs
update-initramfs 

#install smdev
cd ~
git clone git://git.suckless.org/smdev
cd smdev/
make
make install

#install firmwared
apt install libudev1 libudev-dev 
git clone https://github.com/teg/firmwared.git
cd firmwared/
./autogen.sh 
./configure
make
make install

#reboot into sinit
reboot

```